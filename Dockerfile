FROM bitnami/minideb:stretch

COPY conf /tmp/conf
COPY build.sh /tmp/build.sh

# In this first, simple version, we treat the website content as a
# binary component and we make it part of the Docker image itself.
# Just pull the latest version from git and build it inside the image.

# The setup commands are all on a huge single RUN line so that Docker
# does not create temporary container images in between, thus letting
# us effectively recover the unused disk space at the end.
RUN /tmp/build.sh && rm /tmp/build.sh

EXPOSE 80
ENTRYPOINT ["/usr/local/bin/chaperone"]
