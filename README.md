Tutorial: autistici.org on ai3
==============================

Lo scopo di questo tutorial è la realizzazione di una *container
image* che serva il sito autistici.org su HTTP, ed il deployment di
tale immagine sull'infrastruttura ai3. Il sito web è un buon esempio
per impratichirsi poiché è di facile comprensione e non ha dipendenze
da ulteriori servizi.

Nel corso di questo tutorial:
* vedremo quali sono le componenti necessarie a deployare
  un'applicazione sull'infrastruttura *ai3*
* vedremo come si può strutturare un'applicazione affinché sia
  facilmente integrabile con l'infrastruttura
* impareremo ad usare Gitlab per supportare il workflow di deployment
* vedremo un esempio, condotto da cima a fondo, di come tutto quanto
  si incastri assieme fino alla verifica su un ambiente di test
  locale.


# Setup iniziale

Per seguire questo tutorial, la cosa migliore da fare è creare un
proprio repository git su git.autistici.org, nel namespace personale,
per esempio qualcosa come
`git.autistici.org/<utente>/website-docker-tutorial`. Questo
repository inizialmente vuoto diventerà passo passo una copia del
repository di riferimento contenente questo README.

Inoltre bisognerà avere una copia di
https://git.autistici.org/ai3/testbed in locale, per usarlo come
ambiente di test, ed avere seguito le istruzioni nel relativo README
per installarne i prerequisiti.

Per testare e compilare immagini Docker in locale è necessario
[installare Docker](https://www.docker.com/community-edition).


# Definizione del problema

Anzitutto definiamo esattamente il problema da risolvere:
l'applicazione "sito web autistici.org" deve servire, su una porta
HTTP, i contenuti statici del sito principale autistici.org, niente
pannello né webmail né altri contenuti dinamici. Non c'è da
preoccuparsi di SSL, perché viene gestito dal router HTTP globale di
ai3. Da includersi, invece, il motore di ricerca, in quanto parte
integrante del sito stesso.

Sappiamo alcune cose sul sito autistici.org:
* il contenuto è completamente statico ed è generato da un repository
  sorgente (https://git.autistici.org/ai/website) usando un tool
  apposito (*gostatic*)
* il sito deve essere servito da Apache, perché il suo funzionamento
  dipende da funzionalità specifiche di Apache (in particolare il
  modulo *negotiation*)
* sappiamo infine che il motore di ricerca e alcuni tool usati dal
  processo di generazione del sito sono distribuiti in un pacchetto
  Debian chiamato *ai-webtools*.

Dunque si può pensare di strutturare la cosa in questo modo:
* il processo di costruzione dell'immagine scarica il repository del
  sito e genera i contenuti statici
* l'immagine a runtime avvierà Apache ed il demone del motore di
  ricerca – dato che il motore di ricerca è esso stesso parte del sito
  (con la URL `/search`), serviremo tutto da Apache stesso, esponendo
  dunque un'unica porta HTTP all'esterno.

Questa implementazione soddisfa i requisiti, ma non è certo l'unica
possibile: anzi, ne discuteremo dopo vantaggi e svantaggi specifici.

C'è una considerazione importante da fare riguardo al nome di dominio
da usare: l'ambiente di test creato da *testbed* utilizza
*investici.org* come dominio pubblico (per permetterci di testare le
cose più agilmente usando un dominio inutilizzato), dunque useremo
quello per questo tutorial al posto di *autistici.org*. Il sito che
metteremo su, per semplicità, sarà quindi
`https://www.investici.org/`.

# Costruzione dell'immagine

L'infrastruttura *ai3* usa Docker per gestire container, dunque
dobbiamo creare un'immagine Docker. Per far questo è necessario creare
come prima cosa un file con le istruzioni per il build (*Dockerfile*).

## Dockerfile

Un *Dockerfile* è un elenco di istruzioni che Docker usa per costruire
un'immagine, che altro non è che uno snapshot di un filesystem. Le
operazioni disponibili dunque sono fondamentalmente copiare files
dentro l'immagine, ed eseguire comandi (sempre "dentro" l'immagine
stessa).

Per *ai3*, abbiamo scelto, come policy, di usare immagini basate su
Debian, ed installare software nell'immagine usando pacchetti Debian
se possibile. Questo più che altro per semplicità, e perché conosciamo
Debian meglio di altri sistemi, ma anche per aiutarci nel lavoro di
transizione dei servizi attuali.

Vediamo un po' cosa mettere in questo Dockerfile. Anzitutto bisogna
dire a Docker qual è l'immagine *base* da cui partire:

    FROM bitnami/minideb:stretch

Questa non è esattamente l'immagine ufficiale Debian, ma è una sua
versione minimalista che risulta in immagini un poco più piccole.

Poi, copiamo i files che useremo per configurare i servizi *dentro*
l'immagine, in una directory temporanea:

    COPY conf /tmp/conf

Facciamo così invece di copiarli uno per uno, perché non tutte le
destinazioni della copia esistono già a questo punto. L'installazione
delle configurazioni nella loro destinazione finale è demandata allo
script di build, che va anch'esso copiato dentro l'immagine per essere
poi eseguito:

    COPY build.sh /tmp/build.sh

L'utilizzo di uno script di installazione separato non è completamente
ortodosso (di solito si preferisce mettere tutte le istruzioni dentro
al Dockerfile esplicitamente), ma è stato scelto in questo caso perché
la procedura di generazione del sito non è del tutto banale, e risulta
più leggibile in questo modo.

A questo punto possiamo lanciare lo script che installa il software
necessario e genera il sito statico (per educazione rimuoviamo lo
script dopo averlo eseguito):

    RUN /tmp/build.sh && rm /tmp/build.sh

Il Dockerfile si conclude con un paio di direttive che definiscono dei
metadati dell'immagine, in particolare le porte da esporre
all'esterno, ed il comando da eseguire all'avvio:

    EXPOSE 80
    ENTRYPOINT ["/usr/local/bin/chaperone"]

Vedremo poi di cosa si tratta esattamente.


## build.sh

Lo script di installazione deve fare le seguenti cose:
* installare il software necessario a generare il sito e a servirlo
* scaricare il sorgente del sito da git.autistici.org/ai/website
* compilare il sito
* configurare Apache e gli altri servizi che servono

Infine, visto che ci interessa comunque che le immagini siano più
"pulite" e piccole possibile, una volta generato il sito rimuoveremo
il software usato per la compilazione, in quanto non sarà più
necessario a runtime.

I vari passaggi si possono vedere direttamente in
[build.sh](build.sh), che è un po' lungo per riprodurlo qui. In
sostanza si tratta di:

* usiamo *git* per scaricare il sorgente di
  git.autistici.org/ai/website in una directory temporanea
* compiliamo il sito usando il suo script *update.sh*
* copiamo i contenuti generati dalla directory temporanea alla loro
  destinazione finale in `/var/www/investici.org`
* configuriamo Apache per servire il sito `investici.org` (il dominio
  pubblico di test) da quella directory, e per passare /search in
  proxy al demone *sitesearch* su una porta locale

L'immagine risultante contiene due servizi, che vanno eseguiti
contemporaneamente: *apache2* e *sitesearch*. Siccome Docker non è in
grado di controllare più di un processo, ci serve una sorta di
mini-*init* per gestirne l'esecuzione. Una soluzione comoda è un
piccolo programmino chiamato
[Chaperone](http://garywiz.github.io/chaperone/), che purtroppo non ha
un pacchetto Debian e va dunque installato usando *pip3 install*.

## chaperone.conf

Il file di configurazione chaperone.conf contiene una descrizione dei
servizi da avviare. Quando si hanno più processi all'interno di uno
stesso container bisogna farsi delle domande su come vogliamo gestire
gli errori, e quanta funzionalità di *init* vogliamo trasferire
all'interno del container: questa decisione si colloca su una scala ad
un estremo della quale si trovano container simili alle VM, dove
l'init interno si occupa interamente della gestione dei servizi ed il
container in quanto tale è "eterno", all'altro estremo invece abbiamo
i container a singolo processo, che non hanno bisogno di un init, ed
il cui lifecycle è lo stesso del processo (quando il processo termina,
il container esce). Ci sono varie questioni da considerare per fare
una scelta:

* Come ci accorgiamo di problemi tipo servizi che non partono perché
  hanno problemi all'avvio? Se init è *interno* al container, dobbiamo
  monitorarlo in aggiunta a quello esterno, il che comporta
  duplicazione del lavoro.
* Come gestiamo procedure come il riavvio di un servizio? Con un init
  interno, ci serve un modo per raggiungerlo e passargli dei comandi,
  cosa che può diventare alquanto macchinosa.

Dunque in generale è meglio mantenersi sul semplice e cercare di
agganciare il *lifecycle* del container a quello del processo che ci
interessa. Il problema però diventa più complicato quando nel
container c'è più di un servizio: in tal caso ci sono alcune opzioni:

* I processi sono tutti ugualmente importanti: in questo caso ha senso
  terminare l'intero container quando uno di essi ha un
  problema. Questo può essere un po' antipatico perché stiamo
  terminando anche il processo che *funziona*. Del resto se non
  fossero strettamente accoppiati non sarebbero nello stesso
  container.
  
* C'è un servizio fondamentale ed altri diciamo *ausiliari*, nel qual
  caso si può terminare il container quando il processo fondamentale
  termina, si può riavviare silenziosamente un processo ausiliario
  quando questo muore. In questo modo si evitano troppi riavvii del
  processo principale in caso di problemi.

La seconda soluzione assomiglia abbastanza al problema che abbiamo
adesso: Apache è il servizio "complicato" nel nostro caso, mentre il
motore di ricerca è relativamente stupido e poco problematico, quindi
possiamo delegarne la gestione al container stesso.

Il file di configurazione del nostro mini-init dunque definisce due
servizi:

    apache2.service {
      command: "bash -c 'source /etc/apache2/envvars && exec /usr/sbin/apache2 -DFOREGROUND'",
      exit_kills: true,
    }
    
    search.service: {
      command: "/usr/sbin/sitesearch --index=/var/lib/sitesearch/index --templates=/var/lib/sitesearch/templates --http=127.0.0.1:3301",
      uid: www-data,
      restart: true,
    }

Notare che in entrambi i casi non usiamo gli script in /etc/init.d per
avviare i servizi, perché non ce n'è bisogno, e perché chaperone è più
contento se gli diamo dei processi in foreground da gestire.

Per il servizio apache abbiamo specificato `exit_kills: true`
rendendolo così il processo "principale" del container: se apache
esce, il container termina. Il servizio search invece specifica
`restart: true`, perché vogliamo riavviarlo se per caso muore.

## Altri file di configurazione

I rimanenti file di configurazione che ci servono sono relativi ad
Apache, e sono il file con la definizione del VirtualHost (che sarà
comunque l'unico sito configurato in Apache), ed alcuni altri piccoli
file con cui modifichiamo leggermente la configurazione di default di
Apache in Debian.

Questi si trovano dentro `conf/` e non necessitano di tanti commenti.


# Compilare l'immagine in locale

Una volta raccolti questi files (che si possono tranquillamente
copiare da questo repository), si può lanciare Docker e compilare
l'immagine:

    $ docker build -t website-test .

Se tutto va bene ci ritroveremo con un'immagine chiamata
`website-test:latest`, che potremo poi lanciare con:

    $ docker run -p 80 -t -i website-test:latest
    Nov 29 23:17:19 901c67b00325 chaperone[1]: Switching all chaperone logging to /dev/log
    Nov 29 23:17:19 901c67b00325 chaperone[1]: chaperone version 0.3.9, ready.
    Nov 29 23:17:19 901c67b00325 chaperone[1]: system will be killed when '/bin/bash' exits
    Nov 29 23:17:19 901c67b00325 apache2[17]: AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message

A questo punto da un altro terminale si può verificare che il
container risponda ad una richiesta HTTP sulla sua porta 80 (gli
header buffi emulano il comportamento del router HTTP globale ai3):

    $ curl -H 'X-Forwarded-Proto: https' --resolve investici.org:80:172.17.0.2 \
            http://investici.org/

e dovremmo ricevere la homepage del sito.

# Gitlab Container Registry

A questo punto dobbiamo fare in modo che questa immagine venga
compilata automaticamente e resa disponibile all'infrastruttura
*ai3*. Possiamo fare tutto ciò da dentro Gitlab. Anzitutto creiamo un
file per configurare Gitlab-CI per questo repository, dicendogli come
compilare l'immagine Docker. Questo file dovrà chiamarsi
`.gitlab-ci.yml` e dovrà avere il seguente contenuto:

    image: docker:latest

    stages:
      - build
      - release

    services:
      - docker:dind

    variables:
      IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
      RELEASE_TAG: $CI_REGISTRY_IMAGE:latest

    before_script:
      - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.git.autistici.org

    build:
      stage: build
      script:
        - docker build --pull -t $IMAGE_TAG .
        - docker push $IMAGE_TAG

    release:
      stage: release
      script:
        - docker pull $IMAGE_TAG
        - docker tag $IMAGE_TAG $RELEASE_TAG
        - docker push $RELEASE_TAG
      only:
        - master
    
Una volta aggiunti tutti i files menzionati finora (`Dockerfile`,
`build.sh`, `conf/*`, `.gitlab-ci.yml`) al repository, committiamo le
modifiche e mandiamole a Gitlab con `git push -u origin
master`. Gitlab avvierà la pipeline CI e costruirà l'immagine
desiderata. Se tutto va bene, questa sarà disponibile nel Docker
Registry di Gitlab (che richiede autenticazione) come:

    registry.git.autistici.org/<USER>/website-docker-tutorial:latest

Se qualcosa andasse storto nella creazione dell'immagine, Gitlab
dovrebbe inviare una mail. In ogni caso si puo' sempre controllare
lo stato delle pipeline visitando

    https://git.autistici.org/<USER>/website-docker-tutorial/pipelines

E' necessario un ultimo passaggio, per permettere ad ai3 di
accedere al registry: bisogna aggiungere l'utente Gitlab
*docker-registry-client* al progetto, con livello di accesso
"Reporter". Questo si fa da

    https://git.autistici.org/<USER>/website-docker-tutorial/project_members


# Deploy dell'immagine su ai3

Vediamo infine come deployare l'immagine appena costruita
sull'infrastruttura *ai3/testbed*. Il *testbed* ha un ambiente di test
locale basato su Vagrant – useremo quello, modificandolo in locale per
aggiungere il nuovo servizio che abbiamo appena creato.

Spostiamoci dunque nel repository di *ai3/testbed*. Nota: *non
modificare il repository remoto!* Le modifiche descritte di seguito
vanno fatte in locale e non devono essere committate (hanno natura di
sperimentazione temporanea). Per essere sicuri di non generare
modifiche involontarie conviene operare su un *branch*:

    $ cd ai3/testbed  # o dove avete clonato il repository
    $ git checkout -b tutorial-1

Dobbiamo creare un nuovo *servizio*, chiamiamolo *website*, e dire ad
ai3 di esporlo all'esterno come *www.investici.org*. Dunque
modifichiamo `conf/services.yml` (la configurazione dei servizi
dell'ambiente di test) aggiungendo:

    website:
      containers:
        - name: website
          image: registry.git.autistici.org/<USER>/website-docker-tutorial:latest
          port: 8080
      public_endpoints:
        - name: www
          scheme: http
          port: 8080

Il dominio dei *public_endpoints* è implicito, basta specificare il
prefisso desiderato: in questo caso il risultato sarà per l'appunto
*www.investici.org*.

Dopo aver lanciato Ansible (come spiegato nel
[README](https://git.autistici.org/ai3/testbed) di *ai3/testbed*), si
può verificare che tutto funzioni con:

    $ curl -k --resolve www.investici.org:443:192.168.10.10 \
        https://www.investici.org/

che dovrebbe servire la homepage del sito. Come metodo di verifica
alternativo, se si vuole navigare un po' sul sito, si può aggiungere
una riga al proprio `/etc/hosts`:

    192.168.1.10 www.investici.org

ed accedere ad https://www.investici.org/ con il browser. Basta
ricordarsi di rimuovere questa modifica una volta effettuato il test.

Infine, una volta verificato che tutto funziona, bisogna eliminare le
nostre modifiche a services.yml, per esempio scartando il branch:

    $ git checkout master
    $ git branch -D tutorial-1


# Extra: monitoring

Sarebbe utile, visto che il sito autistici.org è abbastanza
importante, esporre anche un endpoint per il monitoraggio di questa
istanza di Apache tramite Prometheus. Anche qui ci sono svariate
possibilità, una di queste è usare un pacchetto chiamato
*apache-exporter*: trattasi di un semplice demone che periodicamente
analizza la pagina di status di Apache, e traduce quelle metriche in
un formato che Prometheus capisce. Questo demone ha dunque la sua
piccola interfaccia HTTP.

Per non dover esporre due porte diverse nel container Docker (non che
sia un problema, è solo per semplicità), possiamo decidere di proxare
le richieste per l'endpoint Prometheus (`/metrics`) da Apache
all'exporter. Questo lo fa il piccolo snippet di configurazione in
`conf/metrics.conf`.

Dobbiamo inoltre dire a Chaperone di avviare il demone:

    exporter.service: {
      command: "/usr/bin/apache_exporter",
      uid: www-data,
      restart: true,
    }

(anche questo, come il motore di ricerca, sarà un servizio
ausiliario).

Infine in `services.yml` dovremo aggiungere, dentro la voce *website*
definita sopra:

      monitoring_endpoints:
        - job_name: website
          port: 8080
          scheme: http



# Considerazioni

Beh adesso che tutto è funzionante sul nostro sistema di test,
supponiamo che qualcuno faccia una modifica al sito (nel repository
ai/website). Noi diligentemente lanciamo di nuovo Ansible, e... non
succede niente. Questo perché non c'è nessun collegamento tra il
repository ai/website e la pipeline CI del nostro repository che crea
l'immagine Docker. Ed in ogni caso, anche se questo collegamento ci
fosse (immagino sia possibile in qualche modo), il risultato sembra
leggermente inefficiente: ogni volta che correggiamo un typo nel sito,
il sistema CI ricrea l'immagine da zero, installa di nuovo Apache con
la stessa identica configurazione, etc. etc.

Ci sono altri schemi ipotizzabili che risolvono questi problemi
differentemente: tanto per cominciare si può accentrare la generazione
dell'immagine ed il sito in un unico repository. Oppure, si può
separare un'immagine *base* con Apache ed usarla come FROM: per
l'immagine del sito. Ancora un'altra possibilità è avere un'immagine
Docker con solo Apache, e distribuire/compilare il sito tramite un
ruolo Ansible. Di volta in volta, la scelta giusta dipenderà
dall'applicazione, ma il ragionamento qui esposto può dare un'idea dei
*trade-off* da considerare.
