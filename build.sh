#!/bin/sh
#
# Install script for git.autistici.org/ai/website
# inside a Docker container.
#
# The installation procedure requires installing some
# dedicated packages, so we have split it out to a script
# for legibility.

# Packages that are only used to build the site. These will be
# removed once we're done.
BUILD_PACKAGES="git rsync gostatic"

# Packages required to serve the website and run the services.
# We have to keep the python3 packages around in order to run
# chaperone (installed via pip).
PACKAGES="apache2 apache-exporter ai-webtools python3-pip python3-setuptools python3-wheel"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends "$@"
    }
fi

set -e

# Install the main A/I package repository.
install_packages curl gnupg
echo "deb http://deb.autistici.org/urepo stretch-ai/" > /etc/apt/sources.list.d/ai.list
curl -s http://deb.autistici.org/repo.key | apt-key add -
apt-get -q update

install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Clone the website source repository, build it and
# copy the results in the web root directory.
mkdir -p /tmp/site
git clone https://git.autistici.org/ai/website.git /tmp/site

(cd /tmp/site && umask 022 && ./scripts/update.sh)

cp -ar /tmp/site/public /var/www/investici.org
cp -ar /tmp/site/index /var/lib/sitesearch/
cp -ar /tmp/site/templates /var/lib/sitesearch/
chown -R www-data /var/lib/sitesearch
rm -fr /tmp/site

# Create the directories that Apache will need at runtime,
# since we won't be using the init script.
mkdir /var/run/apache2 /var/lock/apache2

# Configure Apache.
cp /tmp/conf/investici.org.conf /etc/apache2/sites-available/000-default.conf
cp /tmp/conf/security.conf /etc/apache2/conf-available/security.conf
cp /tmp/conf/metrics.conf /etc/apache2/conf-available/metrics.conf
ln -s ../conf-available/metrics.conf /etc/apache2/conf-enabled/metrics.conf
:> /etc/apache2/conf-available/other-vhosts-access-log.conf

a2enmod -q headers
a2enmod -q rewrite
a2enmod -q negotiation
a2enmod -q proxy
a2enmod -q proxy_http
a2dismod -q ssl
a2dismod -q -f deflate

# Install Chaperone (minimalistic init service).
pip3 install chaperone
rm -fr /root/.cache/pip
mkdir /etc/chaperone.d

cp /tmp/conf/chaperone.conf /etc/chaperone.d/chaperone.conf

# Remove packages used for installation.
apt-get remove -y --purge curl gnupg ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf

